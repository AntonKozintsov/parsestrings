Given two text files: input.txt and patterns.txt, where input.txt is a free-text document composed of 1 or more lines of text, and patterns.txt is a set of search strings (1 per line). Your application should be able to run in one of three different modes:  

Required:  

Output all the lines from input.txt that match exactly any pattern in patterns.txt  


Optional:  


2) output all the lines from input.txt that contain a match from patterns.txt somewhere in the line.  


3) output all the lines from input.txt that contain a match with edit distance <= 1 patterns.txt  
For example
INPUT.TXT:
Hello.  This is line 1 of text.
and this is another.
line 3 here
the end

PATTERNS.TXT
the end
matches
line 3
and this is anoother.


Mode 1 outputs:  
the end

Mode 2 outputs:  
line 3 here  
the end

Mode 3 outputs:  
and this is another.  
the end
