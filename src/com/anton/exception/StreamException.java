package com.anton.exception;

public class StreamException extends Exception {

    public StreamException() {
        super();
    }


    public StreamException(final String message) {
        super(message);
    }


    public StreamException(final Throwable exception) {
        super(exception);
    }


    public StreamException(final String message, final Throwable exception) {
        super(message, exception);
    }
}

