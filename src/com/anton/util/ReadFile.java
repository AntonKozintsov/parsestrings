package com.anton.util;

import com.anton.exception.StreamException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReadFile {

    public static List<String> readData(String path)
            throws StreamException {
        List<String> list;
        try {
            list = Files.lines(Paths.get(path))
                    .flatMap(line -> Stream.of(line.split("\n")))
                    .filter(line->line.matches("(?!\\s*$).+"))
                    .collect(Collectors.toList());
            return list;
        } catch (IOException e) {
            throw new StreamException("problems with file");
        }
    }
}
