package com.anton;

import com.anton.action.FilterLines;
import com.anton.constant.FileConstant;
import com.anton.exception.StreamException;
import com.anton.util.ReadFile;

import java.util.List;

public class Main {

    public static void main(String[] args) throws StreamException {
        List<String> dataLines = ReadFile.readData(FileConstant.INPUT_FILE);
        List<String> patternLines = ReadFile.readData(FileConstant.OUTPUT_FILE);
        FilterLines.filterOptionMatches(dataLines, patternLines);
        FilterLines.filterOptionContains(dataLines, patternLines);
        FilterLines.filterOptionMatchesEdit(dataLines, patternLines);

    }
}

