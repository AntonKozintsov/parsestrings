package com.anton.action;

import java.util.List;

public class FilterLines {

    public static void filterOptionMatches(List<String> dataLines, List<String> patternLines) {
        System.out.println("Result after the matches filtering option:");
        StringBuilder result = new StringBuilder();
        for (String line : dataLines) {
            for (String patternLine : patternLines) {
                if (patternLine.equals(line)) {
                    result.append(line).append("\n");
                    break;
                }
            }
        }
        System.out.println(result);
    }

    public static void filterOptionContains(List<String> dataLines, List<String> patternLines) {
        System.out.println("Result after the contains filtering option");
        StringBuilder result = new StringBuilder();
        for (String pattern : patternLines) {
            for (String line : dataLines) {
                if (line.contains(pattern)) {
                    result.append(line).append("\n");
                }
            }
        }
        System.out.println(result);
    }

        public static void filterOptionMatchesEdit(List<String> dataLines, List<String> patternLines) {
        System.out.println("Result after the Matches<=1 edit filtering option");
        StringBuilder result = new StringBuilder();
        for (String pattern : patternLines) {
            for (String line : dataLines) {
                if (isMatchedInRange(pattern, line)) {
                    result.append(line).append("\n");
                }
            }
        }
        System.out.println(result);
    }
    public static boolean isMatchedInRange(String data, String pattern) {
        if (Math.abs(data.length() - pattern.length()) > 1) {
            return false;
        }
        int countUnmatched = 0;
        if (pattern.length() > data.length()) {
            countUnmatched = getCountUnmatched(data.toCharArray(), pattern.toCharArray(), countUnmatched);
        } else {
            countUnmatched = getCountUnmatched(pattern.toCharArray(), data.toCharArray(), countUnmatched);
        }
        return countUnmatched <= 1;
    }

    private static int getCountUnmatched(char[] dataArr, char[] patternArr, int countUnmatched) {
        for (int i = 0, patternArrIndex = 0; patternArrIndex < patternArr.length; patternArrIndex++) {
            if (patternArr[patternArrIndex] != dataArr[i]) {
                countUnmatched++;
            } else {
                i++;
                if (i > dataArr.length - 1) {
                    i = dataArr.length - 1;
                }
            }
        }
        return countUnmatched;
    }
}
