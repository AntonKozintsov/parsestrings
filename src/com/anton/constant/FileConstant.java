package com.anton.constant;

public class FileConstant {

    private FileConstant() {
    }

    public static final String INPUT_FILE = "data/input.txt";

    public static final String OUTPUT_FILE = "data/patterns.txt";
}
